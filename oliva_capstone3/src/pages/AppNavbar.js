import { useContext, Fragment, useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import Accordion from 'react-bootstrap/Accordion';

export default function AppNavbar(){

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	return(
		<Navbar fixed = "top" bg="dark" expand="lg" variant = "dark">
		<Navbar.Brand as={ Link } to='/'>GizPC</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={ NavLink } to='/'>Home</Nav.Link>
					<Nav.Link as={ NavLink } to='/products'> Products</Nav.Link>
					<Nav.Link className = "d-lg-none" as={ NavLink } to='/login' onClick={handleClose}> Login</Nav.Link>
					<Nav.Link className = "d-lg-none" as={ NavLink } to='/register' onClick={handleClose}> Register</Nav.Link>
				</Nav>
			</Navbar.Collapse>
			<Button className = "d-lg-block" id = "btn-menu" variant="dark" onClick={handleShow} hidden>
			        Menu
			      </Button>
		    <Offcanvas placement = "end" show={show} onHide={handleClose}>
		      <Offcanvas.Header closeButton>
		        <Offcanvas.Title>GizPC</Offcanvas.Title>
		      </Offcanvas.Header>
		      <Offcanvas.Body>
		        <Nav.Link className = "my-2" as={ NavLink } to='/login' onClick={handleClose}> Login</Nav.Link>
		        <Nav.Link className = "my-2" as={ NavLink } to='/register' onClick={handleClose}> Register</Nav.Link>
		      </Offcanvas.Body>
		    </Offcanvas>
		</Navbar>
	)
}