import './App.css';
import Login from "./pages/Login";
import Register from "./pages/Register";
import { UserProvider } from './UserContext';
import AppNavbar from "./pages/AppNavbar";
import { Container } from 'react-bootstrap'
import { useState, useEffect } from 'react';
import { BrowserRouter as Router,Routes, Route, Navigate } from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  },[user])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
      <Container fluid>
      <Routes>
        {/*<Route path='/' element={<Home />} />*/}
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        {/*<Route 
          path='/register' 
          element={
            (user.id===null)? (
              <Register />
              ) : (
                <Navigate to = "/courses"/>
              )
            } 
          />*/}
        {/*<Route path='*' element={<Error />} />*/}
      </Routes>
      </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

